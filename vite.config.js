import { dirname, resolve } from "node:path";
import { fileURLToPath } from "node:url";
import { defineConfig } from "vite";
import dts from "vite-plugin-dts";

const __dirname = dirname(fileURLToPath(import.meta.url));

export default defineConfig({
  plugins: [dts({ rollupTypes: true })],

  build: {
    lib: {
      name: "Vroum",
      fileName: "vroum",
      entry: resolve(__dirname, "lib/index.ts"),
      formats: ["umd", "es"],
    },
  },
});
