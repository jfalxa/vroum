The initial goal of this library was to provide a way to define reusable components that could be painted on a `canvas` at 60fps.

It then evolved into something less specific and now tries to provide the following features:

- Abstract specific behaviors by encapsulating them in reusable node classes.
- Connect nodes as a tree to compose them into more complex behaviors.
- Schedule actions precisely by defining tasks that run on a timed loop.

## Example

The following program will run a loop and log the elapsed time at regular intervals.

```ts
import { Loop, Task } from "vroum";

class LogTask extends Task {
  delay = 500; // wait 500ms before writing the first log
  duration = 1000; // then log on every frame for 1s
  repeat = 3; // repeat the log cycles only 3 times
  interval = 2000; // but wait 2s inbetween cycles

  tick() {
    console.log(`${this.parent.elapsedTime}ms have passed since start.`);
  }
}

// start a new loop
const loop = new Loop();

// schedule the task on that loop
new LogTask(loop);
```

## Installation

Run `npm install vroum` to install the library in your project.

You can them import its contents the regular way: `import { Node, Task, Loop } from "vroum"`
