import { Task } from "./task";

export class Loop extends Task {
  tasks: Task[] = [];

  time: number | undefined;
  lastTime: number | undefined;
  deltaTime = 0;

  maxFPS: number;
  frameTime: number;

  private _frame: number | undefined;

  constructor(maxFPS = 60) {
    super();
    this.maxFPS = maxFPS;
    this.frameTime = 1000 / maxFPS;
  }

  protected mount() {
    this.time = undefined;
    this.lastTime = undefined;
    this.deltaTime = 0;

    this._requestNextFrame();
  }

  protected destroy() {
    this._cancelLastFrame();
  }

  private _requestNextFrame() {
    this._frame = requestAnimationFrame(this._runTasks);
  }

  private _cancelLastFrame() {
    if (this._frame) cancelAnimationFrame(this._frame);
  }

  private _runTasks = (time: number) => {
    this.time = time;
    this.deltaTime = this.time - (this.lastTime ?? time);

    if (this.deltaTime >= this.frameTime - 3 || this.lastTime === undefined) {
      this.lastTime = this.time;

      if (this.running) {
        for (let i = 0; i < this.tasks.length; i++) {
          this.tasks[i].run();
        }
      }
    }

    this._requestNextFrame(); // request the next frame of the loop
  };
}
