import { Loop } from "./loop";
import { Node } from "./node";

declare global {
  interface EventMap {
    [Task.PLAY]: void;
    [Task.PAUSE]: void;
  }
}

export class Task extends Node implements PromiseLike<void> {
  static PLAY = "play-task" as const;
  static PAUSE = "pause-task" as const;

  declare root: Loop;

  delay = 0;
  interval = 0;
  duration = 0;
  frames = 0;
  repeat = Infinity;

  elapsedTime = 0;
  cycleTime = 0;

  cycles = 0;
  ticks = 0;

  running = true;
  done = false;

  private _cycleStartTime = 0;
  private _cycleEndTime = 0;

  protected begin?(): void;
  protected beforeCycle?(): void;
  protected tick?(): void;
  protected afterCycle?(): void;

  protected mount() {
    this.running = true;
    this.done = false;

    this.elapsedTime = 0;
    this.cycleTime = 0;

    this.cycles = 0;
    this.ticks = 0;

    if (this.frames !== 0) {
      this.duration = Math.ceil((this.frames * 1000) / this.root.maxFPS);
    } else if (this.duration !== 0) {
      this.frames = Math.ceil((this.duration * this.root.maxFPS) / 1000);
    }

    this._cycleStartTime = this.delay;
    this._cycleEndTime = this._cycleStartTime + this.duration;

    // add task to root loop
    this.root.tasks.push(this);
  }

  protected destroy() {
    this.running = false;
    this.done = true;

    // remove task from root loop
    const index = this.root.tasks.indexOf(this);
    if (index > -1) this.root.tasks.splice(index, 1);
  }

  play() {
    this.running = true;
    this.emit(Task.PLAY);
  }

  pause() {
    this.running = false;
    this.emit(Task.PAUSE);
  }

  protected shouldTick() {
    const isInstant = this.duration === 0;
    const isDelayPassed = this.elapsedTime >= this._cycleStartTime;
    const isFramesReached = this.ticks >= this.frames;

    return isDelayPassed && (isInstant || !isFramesReached);
  }

  protected shouldEnd() {
    return this.cycles >= this.repeat;
  }

  run() {
    if (!this.running || !this.mounted) return;

    this.elapsedTime += this.root.deltaTime;

    const isFirstTick = this.ticks === 0;
    const isCycleStarted = this.elapsedTime >= this._cycleStartTime;
    const isCycleEnded = this.elapsedTime >= this._cycleEndTime;

    if (this.shouldTick()) {
      if (isCycleStarted && isFirstTick) {
        this.beforeCycle?.();
      }

      if (this.duration > 0) {
        this.cycleTime = this.elapsedTime - this._cycleStartTime;
      }

      this.tick?.();
      this.ticks += 1;
    }

    if (isCycleEnded) {
      this.afterCycle?.();

      this.cycles += 1;
      this.ticks = 0;

      // project next cycle timings
      this._cycleStartTime = this.delay + (this.duration + this.interval) * this.cycles // prettier-ignore
      this._cycleEndTime = this._cycleStartTime + this.duration;
    }

    if (this.shouldEnd()) {
      this.disconnect();
    }
  }

  then<T>(onfulfilled: () => T | PromiseLike<T>): PromiseLike<T> {
    if (this.done) return Promise.resolve(onfulfilled());
    else return new Promise((resolve) => this.once(Task.DESTROY, () => resolve(onfulfilled()))); // prettier-ignore
  }
}
