import { defineConfig } from "vitest/config";

export default defineConfig({
  test: {
    coverage: {
      include: ["lib", "tests"],
      reporter: ["text", "json", "html"],
    },
  },
});
