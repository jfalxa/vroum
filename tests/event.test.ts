import { expect, test, vi } from "vitest";
import { Node } from "../lib/index.ts";

declare global {
  interface EventMap {
    test: [number, { meta: boolean }, string];
    test1: void;
    test2: void;
  }
}

test("register an event and emit it", () => {
  const node = new Node();
  const callback = vi.fn();

  node.on("test", callback);

  expect(callback).not.toHaveBeenCalled();

  node.emit("test");

  expect(callback).toHaveBeenCalledOnce();
});

test("register an event and emit it with some data", () => {
  const node = new Node();
  const callback = vi.fn();

  node.on("test", callback);

  expect(callback).not.toHaveBeenCalled();

  node.emit("test", [1, { meta: false }, "other"]);

  expect(callback).toHaveBeenCalledOnce();
  expect(callback).toHaveBeenLastCalledWith([1, { meta: false }, "other"]);
});

test("remove a specific event callback", () => {
  const node = new Node();
  const callback1 = vi.fn();
  const callback2 = vi.fn();

  node.on("test", callback1);
  node.on("test", callback2);

  expect(callback1).not.toHaveBeenCalled();
  expect(callback2).not.toHaveBeenCalled();

  node.off("test", callback1);
  node.emit("test");

  expect(callback1).not.toHaveBeenCalled();
  expect(callback2).toHaveBeenCalledOnce();
});

test("remove a all the callbacks for an event", () => {
  const node = new Node();
  const callback1 = vi.fn();
  const callback2 = vi.fn();

  node.on("test", callback1);
  node.on("test", callback2);

  expect(callback1).not.toHaveBeenCalled();
  expect(callback2).not.toHaveBeenCalled();

  node.off("test");
  node.emit("test");

  expect(callback1).not.toHaveBeenCalled();
  expect(callback2).not.toHaveBeenCalled();
});

test("clear all event callbacks", () => {
  const node = new Node();
  const callback1 = vi.fn();
  const callback2 = vi.fn();

  node.on("test1", callback1);
  node.on("test2", callback2);

  expect(callback1).not.toHaveBeenCalled();
  expect(callback2).not.toHaveBeenCalled();

  node.off("test1");
  node.off("test2");
  node.emit("test1");
  node.emit("test2");

  expect(callback1).not.toHaveBeenCalled();
  expect(callback2).not.toHaveBeenCalled();
});
