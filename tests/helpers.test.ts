import { expect, test } from "vitest";
import { controlledRequestAnimationFrame } from "./helpers";

const frames = controlledRequestAnimationFrame({ fps: 100 });

test("test requestAnimationFrame helper", async () => {
  const times: number[] = [];

  function timer(time: number) {
    times.push(time);
    timerId = requestAnimationFrame(timer);
  }

  let timerId = requestAnimationFrame(timer);

  expect(timerId).toBe(1);

  expect(times.length).toBe(0);
  expect([...frames.queue.entries()]).toEqual([[1, timer]]);

  frames.run(1);

  expect(timerId).toBe(2);

  expect(times).toEqual([10]);
  expect([...frames.queue.entries()]).toEqual([[2, timer]]);

  frames.run(2);

  expect(times).toEqual([10, 20, 30]);
  expect([...frames.queue.entries()]).toEqual([[4, timer]]);

  expect(timerId).toBe(4);

  function empty() {
    emptyId = requestAnimationFrame(empty);
  }

  // add a second RAF loop
  let emptyId = requestAnimationFrame(empty);

  expect([...frames.queue.entries()]).toEqual([[4, timer], [5, empty]]); // prettier-ignore

  expect(timerId).toBe(4);
  expect(emptyId).toBe(5);

  frames.run(1);

  expect(times).toEqual([10, 20, 30, 40]);
  expect([...frames.queue.entries()]).toEqual([[6, timer], [7, empty]]); // prettier-ignore

  expect(timerId).toBe(6);
  expect(emptyId).toBe(7);

  // cancel the timer loop before executing the frame callbacks
  cancelAnimationFrame(timerId);

  frames.run(1);

  expect(times).toEqual([10, 20, 30, 40]);
  expect([...frames.queue.entries()]).toEqual([[8, empty]]);
});
