import { beforeEach, expect, test, vi } from "vitest";
import { controlledRequestAnimationFrame } from "./helpers.js";
import { Loop, Task } from "../lib/index.ts";

const frames = controlledRequestAnimationFrame({ fps: 100 });

class TestTask extends Task {
  onMount = vi.fn();
  onDestroy = vi.fn();
  mount() {
    this.onMount();
  }
  destroy() {
    this.onDestroy();
  }
  beforeCycle() {}
  tick() {}
  afterCycle() {}
}

beforeEach(() => {
  frames.reset();
});

test("create a task", async () => {
  const loop = new Loop(100);
  const task = new TestTask(loop);

  vi.spyOn(task, "tick");

  await "mounting...";

  expect(task.root).toBe(loop);
  expect(loop.tasks.length).toBe(2);

  frames.run(1); // run initial frame (t=0)
  frames.run(100);

  expect(task.elapsedTime).toBe(1000);
  expect(task.tick).toHaveBeenCalledTimes(101);
  expect(task.cycles).toBe(101);

  loop.disconnect();
});

test("create a task that starts with a delay", async () => {
  const loop = new Loop(100);
  const task = new TestTask(loop);
  task.delay = 100;

  vi.spyOn(task, "tick");

  await "mounting...";

  frames.run(1); // run initial frame (t=0)
  frames.run(9); // Run 9 frames of 10ms

  expect(loop.elapsedTime).toBe(90);
  expect(task.tick).toHaveBeenCalledTimes(0);

  frames.run(1); // Run 1 frame of 10ms -> delay reached

  expect(loop.elapsedTime).toBe(100);
  expect(task.tick).toHaveBeenCalledTimes(1);

  frames.run(1);

  expect(loop.elapsedTime).toBe(110);
  expect(task.tick).toHaveBeenCalledTimes(2);

  loop.disconnect();
});

test("create a task that with an interval between ticks", async () => {
  const loop = new Loop(100);
  const task = new TestTask(loop);
  task.interval = 100;

  vi.spyOn(task, "tick");

  await "mounting...";

  frames.run(1); // run initial frame (t=0)

  expect(task.elapsedTime).toBe(0);
  expect(task.cycles).toBe(1);
  expect(task.tick).toHaveBeenCalledTimes(1);

  frames.run(9); // Run 9 more -> before interval end

  expect(loop.elapsedTime).toBe(90);
  expect(task.cycles).toBe(1);
  expect(task.tick).toHaveBeenCalledTimes(1);

  frames.run(1); // Run 1 more -> new cycle

  expect(loop.elapsedTime).toBe(100);
  expect(task.cycles).toBe(2);
  expect(task.tick).toHaveBeenCalledTimes(2);

  loop.disconnect();
});

test("create a task that ticks for a certain duration in each cycle", async () => {
  const loop = new Loop(100);
  const task = new TestTask(loop);
  task.duration = 200;
  task.interval = 100;

  vi.spyOn(task, "tick");
  vi.spyOn(task, "beforeCycle");
  vi.spyOn(task, "afterCycle");

  await "mounting...";

  expect(task.frames).toBe(20);

  expect(task.ticks).toBe(0);
  expect(task.tick).toHaveBeenCalledTimes(0);
  expect(task.beforeCycle).toHaveBeenCalledTimes(0);

  frames.run(1); // run initial frame (t=0)

  expect(task.ticks).toBe(1);
  expect(task.tick).toHaveBeenCalledTimes(1);
  expect(task.beforeCycle).toHaveBeenCalledTimes(1);

  frames.run(10); // Run 10 frames (half duration)

  expect(task.ticks).toBe(11);

  frames.run(9); // Run 9 frames (almost rest of duration)

  expect(loop.elapsedTime).toBe(190);
  expect(task.cycles).toBe(0);
  expect(task.ticks).toBe(20);
  expect(task.tick).toHaveBeenCalledTimes(20);
  expect(task.beforeCycle).toHaveBeenCalledTimes(1);
  expect(task.afterCycle).toHaveBeenCalledTimes(0);

  frames.run(1); // Run 1 frame (last frame of duration)

  expect(loop.elapsedTime).toBe(200);
  expect(task.cycles).toBe(1);
  expect(task.ticks).toBe(0);
  expect(task.tick).toHaveBeenCalledTimes(20);
  expect(task.beforeCycle).toHaveBeenCalledTimes(1);
  expect(task.afterCycle).toHaveBeenCalledTimes(1);

  frames.run(10); // Run 10 more (interval)

  expect(loop.elapsedTime).toBe(300);
  expect(task.cycles).toBe(1);
  expect(task.ticks).toBe(1);
  expect(task.tick).toHaveBeenCalledTimes(21);
  expect(task.beforeCycle).toHaveBeenCalledTimes(2);
  expect(task.afterCycle).toHaveBeenCalledTimes(1);

  frames.run(20); // Run 20 more (duration)

  expect(loop.elapsedTime).toBe(500);
  expect(task.cycles).toBe(2);
  expect(task.ticks).toBe(0);
  expect(task.tick).toHaveBeenCalledTimes(40);
  expect(task.beforeCycle).toHaveBeenCalledTimes(2);
  expect(task.afterCycle).toHaveBeenCalledTimes(2);

  loop.disconnect();
});

test("create a task that repeats a few times before removing itself", async () => {
  const loop = new Loop(100);
  const task = new TestTask(loop);
  task.delay = 100;
  task.interval = 100;
  task.repeat = 10;

  vi.spyOn(task, "tick");

  await "mounting...";

  frames.run(1); // run initial frame (t=0)

  expect(loop.elapsedTime).toBe(0);
  expect(task.cycles).toBe(0);
  expect(task.tick).toHaveBeenCalledTimes(0);
  expect(task.onDestroy).not.toHaveBeenCalled();

  frames.run(10); // run 10 frames (100ms) => delay is passed

  expect(loop.elapsedTime).toBe(100);
  expect(task.cycles).toBe(1);
  expect(task.tick).toHaveBeenCalledTimes(1);
  expect(task.onDestroy).not.toHaveBeenCalled();

  frames.run(90); // run 90 frames (900ms) => 9 more cycles passed => disconnect

  expect(loop.elapsedTime).toBe(1000);
  expect(task.cycles).toBe(10);
  expect(task.tick).toHaveBeenCalledTimes(10);

  await "destroying...";

  expect(task.parent).toBeUndefined();
  expect(task.onDestroy).toHaveBeenCalledTimes(1);

  loop.disconnect();
});

test("create a task and change its playback state", async () => {
  const loop = new Loop(100);
  const task = new TestTask(loop);

  vi.spyOn(task, "tick");

  await "mounting...";

  frames.run(1); // run initial frame (t=0)

  frames.run(5);

  expect(loop.elapsedTime).toBe(50);
  expect(task.elapsedTime).toBe(50);
  expect(task.cycles).toBe(6);
  expect(task.tick).toHaveBeenCalledTimes(6);

  task.pause();

  frames.run(5);

  expect(loop.elapsedTime).toBe(100);
  expect(task.elapsedTime).toBe(50);
  expect(task.cycles).toBe(6);
  expect(task.tick).toHaveBeenCalledTimes(6);

  task.play();

  frames.run(5);

  expect(loop.elapsedTime).toBe(150);
  expect(task.elapsedTime).toBe(100);
  expect(task.cycles).toBe(11);
  expect(task.tick).toHaveBeenCalledTimes(11);

  loop.disconnect();
});

test("verify tick order", async () => {
  const ticked: number[] = [];

  class IdTask extends TestTask {
    constructor(public id: number, parent?: Task) {
      super(parent);
    }
    tick() {
      ticked.push(this.id);
    }
  }

  const loop = new Loop(100);
  const task1 = new IdTask(1, loop);
  const task1_1 = new IdTask(2, task1);
  const task1_2 = new IdTask(3, task1);
  const task2 = new IdTask(4, loop);
  const task2_1 = new IdTask(5, task2);
  const task2_1_1 = new IdTask(6, task2_1);

  await "mounting...";

  frames.run(1);

  expect(ticked).toEqual([1, 2, 3, 4, 5, 6]);
});

test("await a task", async () => {
  const loop = new Loop(100);
  const task = new TestTask(loop);
  task.duration = 1000;
  task.repeat = 1;

  await "mounting...";

  frames.run(1); // initial frame

  let done = false;
  task.then(() => (done = true));

  expect(done).toBe(false);

  frames.run(99);
  await "microtask";

  expect(done).toBe(false);

  frames.run(1);
  await "microtask";

  expect(done).toBe(true);

  let done2 = false;
  task.then(() => (done2 = true));

  await "microtask";

  expect(done2).toBe(true);
});

test("create a task that ticks for a specific amount of frames", async () => {
  const loop = new Loop(100);
  const task = new TestTask(loop);
  task.frames = 10;
  task.interval = 100;

  await "mounting...";

  frames.run(1); // initial frame

  expect(task.elapsedTime).toBe(0);
  expect(task.ticks).toBe(1);
  expect(task.cycles).toBe(0);

  frames.run(9); // run until before-the-last frame

  expect(task.elapsedTime).toBe(90);
  expect(task.ticks).toBe(10);
  expect(task.cycles).toBe(0);

  frames.run(1); // run last frame

  expect(task.elapsedTime).toBe(100);
  expect(task.ticks).toBe(0);
  expect(task.cycles).toBe(1);

  frames.run(10); // run interval

  expect(task.elapsedTime).toBe(200);
  expect(task.ticks).toBe(1);
  expect(task.cycles).toBe(1);

  frames.run(9); // run until before-the-last frame

  expect(task.elapsedTime).toBe(290);
  expect(task.ticks).toBe(10);
  expect(task.cycles).toBe(1);

  frames.run(1); // run last frame

  expect(task.elapsedTime).toBe(300);
  expect(task.ticks).toBe(0);
  expect(task.cycles).toBe(2);
});
