import { expect, test, vi } from "vitest";
import { Node } from "../lib/index.ts";

test("create a new node", () => {
  expect(new Node()).toBeDefined();
});

test("create a node with a mount() callback", async () => {
  class Test extends Node {
    ready = false;
    mount() {
      this.ready = true;
    }
  }

  const root = new Test();
  const child1 = new Test(root);

  expect(root.root).toBe(root);
  expect(child1.root).toBe(root);

  expect(root.ready).toBe(false);
  expect(child1.ready).toBe(false);

  await "mounting...";

  expect(root.ready).toBe(true);
  expect(child1.ready).toBe(true);

  const child2 = new Test(child1);

  expect(child2.root).toBe(root);
  expect(child2.ready).toBe(false);

  await "mounting...";

  expect(child2.root).toBe(root);
  expect(child2.ready).toBe(true);
});

test("verify mount order", async () => {
  const mounted: number[] = [];

  class Test extends Node {
    constructor(public id: number, parent?: Node) {
      super(parent);
    }
    mount() {
      mounted.push(this.id);
    }
  }

  const root = new Test(0);
  const child1 = new Test(1, root);
  const child1_1 = new Test(2, child1);
  const child1_2 = new Test(3, child1);
  const child2 = new Test(4, root);
  const child2_1 = new Test(5, child2);
  const child2_1_1 = new Test(6, child2_1);
  const child2_1_2 = new Test(7, child2_1);
  const child2_2 = new Test(8, child2);

  expect(mounted).toHaveLength(0);

  await "mounting...";

  expect(mounted).toHaveLength(9);
  expect(mounted).toEqual([0, 1, 2, 3, 4, 5, 6, 7, 8]);
});

test("create a node with a destroy() callback", async () => {
  class Test extends Node {
    done = false;
    destroy() {
      this.done = true;
    }
  }

  const root = new Test();
  const child1 = new Test(root);
  const child2 = new Test(root);
  const child3 = new Test(child2);

  expect(root.done).toBe(false);
  expect(child1.done).toBe(false);
  expect(child2.done).toBe(false);

  await child1.disconnect();

  expect(root.done).toBe(false);
  expect(child1.done).toBe(true);
  expect(child2.done).toBe(false);
  expect(child3.done).toBe(false);

  await root.disconnect();

  expect(root.done).toBe(true);
  expect(child1.done).toBe(true);
  expect(child2.done).toBe(true);
  expect(child3.done).toBe(true);
});

test("verify destroy order", async () => {
  const destroyed: number[] = [];

  class Test extends Node {
    constructor(public id: number, parent?: Node) {
      super(parent);
    }
    destroy() {
      destroyed.push(this.id);
    }
  }

  const root = new Test(0);
  const child1 = new Test(1, root);
  const child1_1 = new Test(2, child1);
  const child1_2 = new Test(3, child1);
  const child2 = new Test(4, root);
  const child2_1 = new Test(5, child2);
  const child2_1_1 = new Test(6, child2_1);
  const child2_1_2 = new Test(7, child2_1);
  const child2_2 = new Test(8, child2);

  expect(destroyed).toHaveLength(0);

  await root.disconnect();

  expect(destroyed).toHaveLength(9);
  expect(destroyed).toEqual([0, 1, 2, 3, 4, 5, 6, 7, 8]);
});

test("connect a node to a different parent", async () => {
  class Test extends Node {
    onMount = vi.fn();
    onDestroy = vi.fn();
    mount() {
      this.onMount();
    }
    destroy() {
      this.onDestroy();
    }
  }

  const parent1 = new Test();
  const parent2 = new Test();

  const child = new Test(parent1);

  await "mounting...";

  expect(child.parent).toBe(parent1);
  expect(child.onMount).toHaveBeenCalledTimes(1);
  expect(child.onDestroy).toHaveBeenCalledTimes(0);

  child.connect(parent2);

  await "mounting...";

  expect(child.parent).toBe(parent2);
  expect(child.onMount).toHaveBeenCalledTimes(2);
  expect(child.onDestroy).toHaveBeenCalledTimes(1);
});
