export { controlledRequestAnimationFrame };

type RafOptions = {
  fps: number;
};

function controlledRequestAnimationFrame(options: RafOptions) {
  const queue = new Map();
  const frameTime = 1000 / options.fps;

  let callbackIds = 0;
  let time = 0;

  globalThis.requestAnimationFrame = (callback) => {
    const callbackId = ++callbackIds;
    queue.set(callbackId, callback);
    return callbackId;
  };

  globalThis.cancelAnimationFrame = (callbackId) => {
    queue.delete(callbackId);
  };

  function run(frames: number) {
    for (let i = 0; i < frames; i++) {
      time += frameTime;
      const callbacks = [...queue.values()];
      queue.clear();

      for (const callback of callbacks) {
        callback(time);
      }
    }
  }

  function reset() {
    time = 0;
    callbackIds = 0;
    queue.clear();
  }

  return {
    run,
    reset,
    queue,
  };
}
