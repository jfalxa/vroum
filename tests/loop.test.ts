import { beforeEach, expect, test, vi } from "vitest";
import { controlledRequestAnimationFrame } from "./helpers";
import { Loop } from "../lib/index.ts";

const frames = controlledRequestAnimationFrame({ fps: 100 });

class TestLoop extends Loop {
  tick() {}
}

beforeEach(() => {
  frames.reset();
});

test("test loop controls", async () => {
  const loop = new TestLoop(100);

  vi.spyOn(loop, "tick");
  vi.spyOn(loop as any, "_runTasks");
  const runTasks = (loop as any)._runTasks;

  await "mounting...";

  expect(loop.tasks).toEqual([loop]);

  frames.run(1); // initial frame (t=0)

  expect(runTasks).toHaveBeenCalledTimes(1);
  expect(loop.tick).toHaveBeenCalledTimes(1);
  expect(loop.elapsedTime).toBe(0);

  frames.run(10);

  expect(runTasks).toHaveBeenCalledTimes(11);
  expect(loop.tick).toHaveBeenCalledTimes(11);
  expect(loop.elapsedTime).toBe(100);

  loop.pause();

  frames.run(10);

  expect(runTasks).toHaveBeenCalledTimes(21);
  expect(loop.tick).toHaveBeenCalledTimes(11);
  expect(loop.elapsedTime).toBe(100);

  loop.play();

  frames.run(10);

  expect(runTasks).toHaveBeenCalledTimes(31);
  expect(loop.tick).toHaveBeenCalledTimes(21);
  expect(loop.elapsedTime).toBe(200);
});
